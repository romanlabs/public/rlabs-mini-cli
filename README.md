<div style="text-align: center;">
<img src="images/logo.png" alt="Demo" width="300">
</div>


# Table of Contents
- [RLabs Mini CLI](#rlabs-mini-cli)
- [Installation](#installation)
- [Usage](#usage)
  - [Decorating a function to be the app's entry point](#decorating-a-function-to-be-the-apps-entry-point)
  - [Accesing Environment Variables](#accesing-environment-variables)
  - [Accesing CLI args](#accesing-cli-args)
  - [Logging](#logging)
  - [Colours](#colours)
- [Complete example](#complete-example)
- [License](#license)
- [Copyright](#copyright)


## RLabs Mini CLI

RLabs Mini CLI is a simple Python library for creating CLI apps (i.e. CLI commands). Intended for quick prototyping.

# Installation

```bash
pip3 install rlabs-mini-cli
```

# Usage

### Decorating a function to be the app's entry point

A function needs to be specified as the app's entry point using the decorator `@app`. The decorated function must received 3 parameters:

```python
import logging
import argparse

from rlabs_mini_cli import cli
from rlabs_mini_cli import envvars

@cli.app
def myapp(
    args: argparse.Namespace,
    envs: envvars.EnvVars,
    logger: logging.Logger
):
    pass
```

- `args` contains all the received cli arguments
- `envs` containes all the received envvars (only if defined in `envvars_required`) 
- `logger`is the built-in logger ready to use.

### Accesing Environment Variables

Environment variables can be accessed via the param `envvars`, but only those defined in `required_envars`. Environment variable names are lowercased so the var `VAR1` will be accesed as `envvars.var1`:

![Demo](images/envvars.gif)

### Accesing CLI args

CLI args can be accessed via the param `args`, as defined in `args_required`. Hyphens in argument names will be converted to underscore so the cli arg `arg-3` will be accesed via `args.arg_3`. Under the hood this uses the Python argsparse library, but it is purposefully kept simple so only non-optional string parameters are supported.


![Demo](images/args.gif)

### Logging

The built-in logger (which uses the [Rich library](https://github.com/Textualize/rich), hence the colourful logs) can be configured via the parameter `logger_config`: 

![Demo](images/logging.gif)

Please note that the built-in logger (the one passed to the registered app entry point) can be overriden via the argument `logger_override`

```python
myapp = cli.CliApp(
    ...
    logger_override=my_custom_logger
    )
```

### Colours

Colours for the app name, app description, and banner can be changed from parameters as well. They map to [the Rich's Console](https://github.com/Textualize/rich?tab=readme-ov-file#using-the-console) so any colour accepted by the Rich console will work:

```python
myapp = cli.CliApp(
    name="MyApp",
    description="MyApp is an amazing CLIApp",
    banner=myapp_banner,
    name_colour="white on red",
    banner_colour="green",
    description_colour="white",
    ...
)
```

## Complete example

Sample code

```python
import logging
import argparse

from rlabs_mini_cli import cli
from rlabs_mini_cli import envvars

myapp_banner = r"""
  __  __
 |  \/  |         /\
 | \  / |_   _   /  \   _ __  _ __
 | |\/| | | | | / /\ \ | '_ \| '_ \
 | |  | | |_| |/ ____ \| |_) | |_) |
 |_|  |_|\__, /_/    \_\ .__/| .__/
          __/ |        | |   | |
         |___/         |_|   |_|
"""

@cli.app
def myapp(
    args: argparse.Namespace,
    envs: envvars.EnvVars,
    logger: logging.Logger
):
    '''
        CLI App
    '''
    logger.info("Hey there, this is MyApp")
    logger.info(f"args: {args}")
    logger.info(f"envvars: {envs}")

    logger.info(f"envvar1: {envs.envvar1}")
    logger.info(f"env_var2: {envs.env_var2}")
    logger.info(f"arg1: {args.arg1}")
    logger.info(f"arg_2: {args.arg_2}")
    logger.info(f"arg-3: {args.arg_3}")

def main():
    '''
        main
    '''
    myapp = cli.CliApp(
        name="MyApp",
        description="MyApp is an amazing CLIApp",
        banner=myapp_banner,
        name_colour="white on red",
        banner_colour="green",
        description_colour="white",
        envvars_required=[
            "ENVVAR1",
            "ENV_VAR2"
        ],
        args_required=[
            "arg1",
            "arg_2",
            "arg-3"
        ],
        logger_config={
            "level": logging.INFO,
            "format": "%(message)s",
            "show_time": False,
            "words_to_highlight": [
                "hello",
                "there"
            ]
        }
    )
    myapp.run()
```

Sample App run:

```bash
export ENVVAR1=value1 
export ENV_VAR2=value2 
python3 myapp.py --arg1 value1 --arg_2 value2 --arg-3 value3 
```

Output

```bash
Welcome to MyApp

MyApp is an amazing CLIApp

  __  __
 |  \/  |         /\
 | \  / |_   _   /  \   _ __  _ __
 | |\/| | | | | / /\ \ | '_ \| '_ \
 | |  | | |_| |/ ____ \| |_) | |_) |
 |_|  |_|\__, /_/    \_\ .__/| .__/
          __/ |        | |   | |
         |___/         |_|   |_|


INFO     Hey there, this is MyApp                                                              manual_test_run.py:27
INFO     args: Namespace(arg1='value1', arg_2='value2', arg_3='value3')                        manual_test_run.py:28
INFO     envvars: EnvVar(envvar1=value1), EnvVar(env_var2=value2)                              manual_test_run.py:29
INFO     envvar1: value1                                                                       manual_test_run.py:31
INFO     env_var2: value2                                                                      manual_test_run.py:32
INFO     arg1: value1                                                                          manual_test_run.py:33
INFO     arg_2: value2                                                                         manual_test_run.py:34
INFO     arg-3: value3                                                                         manual_test_run.py:35
```

# License

This project is licensed under the GNU Lesser General Public License v3.0 - see the [LICENSE](./LICENSE) file for details.

# Copyright

Copyright (C) 2024 RomanLabs, Rafael Roman Otero