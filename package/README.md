## RLabs Mini CLI

RLabs Mini CLI is a simple Python library for creating CLI apps (i.e. CLI commands). Intended for quick prototyping.

[Project in Gitlab](https://gitlab.com/romanlabs/public/rlabs-mini-cli)

